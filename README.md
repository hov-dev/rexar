![logo](https://raw.githubusercontent.com/thehovdev/rexar/master/public/images/github-logo.png)

<h3>Rexar is all in one javascript framework, with configured and included </h3>
<ul>
    <li>React</li>
    <li>React Router</li>
    <li>Redux</li>
    <li>Webpack (Client / Server)</li>
    <li>NodeJs [dependency]</li>
    <li>MongoDB [dependency]</li>
    <li>Mongoose</li>
    <li>Express</li>
    <li>EJS template engine</li>
    <li>Node-config configuration package</li>
</ul>
<hr>
<h3>Rexar cli</h3>
<h5>Create new react component</h5>
<p>node rexar make:component mycomponent</p>
<h5>Create new redux action</h5>
<p>node rexar make:action myaction</p>
<h5>Create new redux container</h5>
<p>node rexar make:container mycontainer</p>
<h5>Create new redux reducer</h5>
<p>node rexar make:reducer myreducer</p>
<h5>Create new express controller</h5>
<p>node rexar make:controller authController</p>
<h5>Create new express route</h5>
<p>node rexar make:route auth</p>
<h5>Create new express model</h5>
<p>node rexar make:model user</p>
<hr>
<h3>Project Structure</h3>
<h4>Front-end</h4>
<p><b>public/js</b> - main js folder</p>
<p><b>public/js/index.js</b> - entry js file</p>
<p><b>components</b> - all your react components</p>
<p><b>actions</b> - all your redux actions</p>
<p><b>reducers</b> - all your redux reducers</p>
<p><b>containers</b> - all you redux containers</p>
<p><b>routes</b> - all you react components and routes</p>
<p><b>public/images</b> - application images folder</p>
<p><b>public/css</b> - application styles folder</p>
<p><b>views</b> - application views (ejs templates)</p>
<h4>Back-end</h4>
<p><b>dist/index.js</b> - entry node.js file</p>
<p><b>dist/routes</b> - list of all routes</p>
<p><b>dist/controllers</b> - list of all controllers</p>
<p><b>dist/models</b> - list of all models</p>
<p><b>dist/database/connect</b> - connect to mongodb with mongoose</p>
<p><b>dist/includes/app</b> - app settings</p>

<h4>Config file</h4>
<p><b>config/default.json</b> - main config file</p>
<hr>
<h3>Run project</h3>
<p><b>npm install</b></p>
<p><b>npm run watch</b></p>
<i>start code and make something amazing</i>
